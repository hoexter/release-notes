<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
<!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-information" lang="en">
  <title>Issues to be aware of for &releasename;</title>

  <para>
    Sometimes, changes introduced in a new release have side-effects
    we cannot reasonably avoid, or they expose
    bugs somewhere else. This section documents issues we are aware of.  Please also
    read the errata, the relevant packages' documentation, bug reports, and other
    information mentioned in <xref linkend="morereading"/>.
  </para>

  <section id="upgrade-specific-issues">
    <title>Upgrade specific items for &releasename;</title>
    <para>
      This section covers items related to the upgrade from
      &oldreleasename; to &releasename;.
    </para>

    <section id="placeholder" condition="fixme">
      <!-- buster to bullseye -->
      <title>Something</title>
      <para>
	With text
      </para>
    </section>

    <section id="new-vaapi-default-driver">
      <!-- buster to bullseye -->
      <title>New VA-API default driver for Intel GPUs</title>
      <para>
	For Intel GPUs available with Broadwell and newer, the Video Acceleration
	API (VA-API) implementation now defaults to
	<systemitem role="package">intel-media-va-driver</systemitem> for hardware
	accelerated video decoding. Systems which have
	<systemitem role="package">va-driver-all</systemitem> installed will
	automatically be upgraded to the new driver.
      </para>
      <para>
	The legacy driver package <systemitem role="package">i965-va-driver</systemitem>
	is still available and offers support up to the Cannon Lake micro
	architecture. To prefer the legacy driver over the new default one, set
	the environment variable <literal>LIBVA_DRIVER_NAME</literal> to
	<literal>i965</literal>, for instance by setting the variable in
	<filename>/etc/environment</filename>. For more information, please see
	the Wiki's page on
	<ulink url="https://wiki.debian.org/HardwareVideoAcceleration">hardware
	video acceleration</ulink>.
      </para>
    </section>

    <section id="xfs-removes-mount-options">
      <title>The XFS file system no longer supports barrier/nobarrier
      option</title>
      <para>
	Support for the <literal>barrier</literal> and
	<literal>nobarrier</literal> mount options has been removed from
	the XFS file system. It is recommended to check
	<filename>/etc/fstab</filename> for the presence of either
	keyword and remove it. Partitions using these options will fail
	to mount.
      </para>
    </section>

    <section id="security-archive">
      <!-- buster to bullseye -->
      <title>Changed security archive layout</title>
      <para>
	For bullseye, the security suite is now named
	<literal>bullseye-security</literal> instead of
	<literal>buster/updates</literal> and users should adapt their
	APT source-list files accordingly when upgrading.
      </para>
      <para>
	The security line in your APT configuration may look like:
	<programlisting>deb https://deb.debian.org/debian-security bullseye-security main contrib</programlisting>
      </para>
    </section>

    <section id="pam-default-password">
      <!-- buster to bullseye -->
      <title>Password hashing uses yescrypt by default</title>
      <para>
	The default password hash for local system accounts <ulink
	url="https://tracker.debian.org/news/1226655/accepted-pam-140-3-source-into-unstable/">
	has been changed</ulink> from SHA-512 to <ulink
	url="https://www.openwall.com/yescrypt/">yescrypt</ulink> (see
	<ulink url="&url-man;/bullseye/libcrypt-dev/crypt.5.html">
	crypt(5)</ulink>). This is expected to provide improved
	security against dictionary-based password guessing attacks,
	in terms of both the space and time complexity of the attack.
      </para>
      <para>
	To take advantage of this improved security, change local
	passwords; for example use the <command>passwd</command>
	command.
      </para>
      <para>
	Old passwords will continue to work using whatever password hash
	was used to create them.
      </para>
      <para>
	Yescrypt is not supported by Debian 10 (buster). As a result,
	shadow password files (<filename>/etc/shadow</filename>) cannot be
	copied from a bullseye system back to a buster system.  If these
	files are copied, passwords that have been changed on the bullseye
	system will not work on the buster system.  Similarly, password
	hashes cannot be cut&amp;pasted from a bullseye to a buster system.
      </para>
      <para>
	If compatibility is required for password hashes between bullseye
	and buster, modify
	<filename>/etc/pam.d/common-password</filename>. Find the line
	that looks like:
	<programlisting>
password [success=1 default=ignore] pam_unix.so obscure yescrypt
	</programlisting>
	and replace <literal>yescrypt</literal> with <literal>sha512</literal>.
      </para>
    </section>

    <section id="nss-nis">
      <!-- buster to bullseye -->
      <title>NSS NIS and NIS+ support require new packages</title>
      <para>
	<acronym>NSS NIS</acronym> and <acronym>NIS+</acronym> support
	has been moved to separate packages called <systemitem
	role="package">libnss-nis</systemitem> and <systemitem
	role="package">libnss-nisplus</systemitem>. Unfortunately,
	<systemitem role="package">glibc</systemitem> can't depend on
	those packages, so they are now only recommended.
      </para>
      <para>
	On systems using <acronym>NIS</acronym> or
	<acronym>NIS+</acronym>, it is therefore recommended to check
	that those packages are correctly installed after the upgrade.
      </para>
    </section>

    <section id="unbound-config-file-handling">
      <title>Config file fragment handling in unbound</title>
      <para>
	The DNS resolver <systemitem role="package">unbound</systemitem>
	has changed the way it handles configuration file fragments. If
	you are relying on an <literal>include:</literal> directive to
	merge several fragments into a valid configuration, you should
	read <ulink
	url="https://sources.debian.org/src/unbound/bullseye/debian/NEWS/">the
	NEWS file</ulink>.
      </para>
    </section>

    <section id="rsync-parameter-deprecation">
      <title>rsync parameter deprecation</title>
      <para>
	The <systemitem role="package">rsync</systemitem> parameters
	<literal>--copy-devices</literal> and <literal>--noatime</literal>
	have been renamed to <literal>--write-devices</literal> and
	<literal>--open-noatime</literal>. The old forms are no longer
	supported; if you are using them you should see <ulink
	url="https://sources.debian.org/src/rsync/bullseye/debian/rsync.NEWS/">the
	NEWS file</ulink>. Transfer processes between systems running different
	Debian releases may require the buster side to be upgraded to a version
	of <systemitem role="package">rsync</systemitem> from the <ulink
	url="https://backports.debian.org/backports">backports</ulink>
	repository.
      </para>
    </section>

    <section id="vim-addons">
      <title>Vim addons handling</title>
      <para>
	The addons for <systemitem role="package">vim</systemitem>
	historically provided by <systemitem
	role="package">vim-scripts</systemitem> are now managed by Vim's
	native <quote>package</quote> functionality rather than by
	<systemitem role="package">vim-addon-manager</systemitem>. Vim
	users should prepare before upgrading by following the
	instructions in <ulink
	url="https://sources.debian.org/src/vim-scripts/bullseye/debian/NEWS/">the
	NEWS file</ulink>.
      </para>
    </section>

    <section id="openstack-cgroups">
      <!-- buster to bullseye -->
      <title>OpenStack and cgroups v1</title>
      <para>
	OpenStack Victoria (released in bullseye) requires cgroup v1 for
	block device QoS. Since bullseye also changes to using cgroupv2
	by default (see <xref linkend="cgroupv2"/>), the sysfs tree in
	<filename>/sys/fs/cgroup</filename> will not include cgroup v1
	features such as <filename>/sys/fs/cgroup/blkio</filename>, and
	as a result <command>cgcreate -g blkio:foo</command> will
	fail. For OpenStack nodes running <systemitem
	role="package">nova-compute</systemitem> or <systemitem
	role="package">cinder-volume</systemitem>, it is strongly
	advised to add the parameters
	<literal>systemd.unified_cgroup_hierarchy=false</literal> and
	<literal>systemd.legacy_systemd_cgroup_controller=false</literal>
	to the kernel command line in order to override the default and
	restore the old cgroup hierarchy.
      </para>
    </section>

    <section id="openstack-policy-file">
      <!-- buster to bullseye -->
      <title>OpenStack API policy files</title>
      <para>
        Following upstream's recommendations, OpenStack Victoria as
        released in bullseye switches the OpenStack API to use the new
        YAML format. As a result, most OpenStack services, including
        Nova, Glance, and Keystone, appear broken with all of the API
        policies written explicitly in the <filename>policy.json</filename>
        files. Therefore, packages now come with a folder
        <filename>/etc/PROJECT/policy.d</filename>
        containing a file <filename>00_default_policy.yaml</filename>,
        with all of the policies commented out by default.
      </para>
      <para>
        To avoid the old <filename>policy.json</filename> file staying
        active, the Debian OpenStack packages now rename that file as
        <filename>disabled.policy.json.old</filename>. In some cases
        where nothing better could be done in time for the release the
        <filename>policy.json</filename> is even simply deleted. So
        before upgrading, it is strongly advised to back up the
        <filename>policy.json</filename> files of your deployments.
      </para>
      <para>
        More details are available in the
        <ulink url="https://governance.openstack.org/tc/goals/selected/wallaby/migrate-policy-format-from-json-to-yaml.html">
        upstream documentation</ulink>.
      </para>
    </section>

    <section>
      <title>sendmail downtime during upgrade</title>
      <para>
	In contrast to normal upgrades of <systemitem
	role="package">sendmail</systemitem>, during the upgrade of
	buster to bullseye the sendmail service will be stopped, causing
	more downtime than usual. For generic advice on reducing
	downtime see <xref linkend="services-downtime"/>.
      </para>
    </section>

    <section id="fuse3">
      <!-- buster to bullseye -->
      <title>FUSE 3</title>
      <para>
        Some packages including <systemitem role="package">gvfs-fuse</systemitem>,
        <systemitem role="package">kio-fuse</systemitem>, and
        <systemitem role="package">sshfs</systemitem> have switched to FUSE 3.
        During upgrades, this will cause <systemitem role="package">fuse3
        </systemitem> to be installed and <systemitem role="package">fuse
        </systemitem> to be removed.
      </para>
      <para>
        In some exceptional circumstances, e.g., when performing the upgrade by
        only running <command>apt-get dist-upgrade</command> instead of the
        recommended upgrade steps from <xref linkend="ch-upgrading"/>, packages
        depending on <systemitem role="package">fuse3</systemitem> might be
        kept back during upgrades. Running the steps discussed in <xref
        linkend="upgrading-full"/> again with bullseye's <systemitem role="package">
        apt</systemitem> or upgrading them manually will resolve the situation.
      </para>
    </section>

    <section id="gnupg-options">
      <!-- buster to bullseye -->
      <title>GnuPG options file</title>
      <para>
	Starting with version 2.2.27-1, per-user configuration of the
	<literal>GnuPG</literal> suite has completely moved to
	<filename>~/.gnupg/gpg.conf</filename>, and
	<filename>~/.gnupg/options</filename> is no longer in use.
	Please rename the file if necessary, or move its contents to
	the new location.
      </para>
    </section>

    <section id="before-first-reboot">
      <title>Things to do post upgrade before rebooting</title>
      <!-- If there is nothing to do -->
      <para>
	When <literal>apt full-upgrade</literal> has finished, the
	<quote>formal</quote> upgrade is complete.  For the upgrade to
	&releasename;, there are no special actions needed before
	performing a reboot.
      </para>
      <!-- If there is something to do -->
      <para condition="fixme">
	When <literal>apt full-upgrade</literal> has finished, the <quote>formal</quote> upgrade
	is complete, but there are some other things that should be taken care of
	<emphasis>before</emphasis> the next reboot.
      </para>

      <programlisting condition="fixme">
	add list of items here
	<!--itemizedlist>
            <listitem>
            <para>
            bla bla blah
            </para>
            </listitem>
	    </itemizedlist-->
      </programlisting>
    </section>
  </section>

  <section id="not-upgrade-only">
    <title>Items not limited to the upgrade process</title>
    <section id="limited-security-support">
      <title>Limitations in security support</title>
      <para>
	There are some packages where Debian cannot promise to provide
	minimal backports for security issues.  These are covered in the
	following subsections.
      </para>
      <note>
	<para>
	  The package <systemitem
	  role="package">debian-security-support</systemitem> helps to track the
	  security support status of installed packages.
	</para>
      </note>

      <section id="browser-security">
	<!-- Check if this still matches the view of the security team -->
	<title>Security status of web browsers and their rendering engines</title>
	<para>
	  Debian &release; includes several browser engines which are affected by a
	  steady stream of security vulnerabilities. The high rate of
	  vulnerabilities and partial lack of upstream support in the form of long
	  term branches make it very difficult to support these browsers and
	  engines with backported security fixes.  Additionally, library
	  interdependencies make it extremely difficult to update to newer upstream
	  releases. Therefore, browsers built upon e.g. the webkit and khtml
	  engines<footnote><para>These engines are shipped in a number of different
	  source packages and the concern applies to all packages shipping
	  them. The concern also extends to web rendering engines not explicitly
	  mentioned here, with the exception of <systemitem
	  role="source">webkit2gtk</systemitem>.</para></footnote> are included in
	  &releasename;, but not
	  covered by security support. These browsers should not be used against
	  untrusted websites.
	  The <systemitem role="source">webkit2gtk</systemitem> source package is
	  covered by security support.
	</para>
	<para>
	  For general web browser use we recommend Firefox or Chromium.
	  They will
	  be kept up-to-date by rebuilding the current ESR releases for
	  stable.
	  The same strategy will be applied for Thunderbird.
	</para>
      </section>

      <section id="openjdk-17">
	<title>OpenJDK 17</title>
	<para>
	  Debian bullseye comes with an early access version of
	  <literal>OpenJDK 17</literal> (the next expected
	  <literal>OpenJDK LTS</literal> version after <literal>OpenJDK
	  11</literal>), to avoid the rather tedious bootstrap
	  process. The plan is for <literal>OpenJDK 17</literal> to
	  receive an update in bullseye to the final upstream release
	  announced for October 2021, followed by security updates on a
	  best effort basis, but users should not expect to see updates
	  for every quarterly upstream security update.
	</para>
      </section>
    </section>

    <section id="g-c-c-and-orca">
      <!-- buster to bullseye -->
      <title>Accessing GNOME Settings app without mouse</title>
      <para>
	Without a pointing device, there is no direct way to change settings in
	the GNOME Settings app provided by <systemitem
	role="package">gnome-control-center</systemitem>. As a work-around, you
	can navigate from the sidebar to the main content by pressing the
	<keycap>Right Arrow</keycap> twice. To get back to the sidebar, you can
	start a search with <keycombo
	action='simul'><keycap>Ctrl</keycap><keycap>F</keycap></keycombo>, type
	something, then hit <keycap>Esc</keycap> to cancel the search. Now you
	can use the <keycap>Up Arrow</keycap> and <keycap>Down Arrow</keycap> to
	navigate the sidebar. It is not possible to select search results with
	the keyboard.
      </para>
    </section>

    <section>
      <title>
	The <literal>rescue</literal> boot option is unusable without a
	root password
      </title>
      <para>
	With the implementation of <literal>sulogin</literal> used since
	buster, booting with the <literal>rescue</literal> option always
	requires the root password. If one has not been set, this makes
	the rescue mode effectively unusable. However it is still
	possible to boot using the kernel parameter
	<literal>init=/sbin/sulogin --force</literal>
      </para>
      <para>
	To configure systemd to do the equivalent of this whenever it
	boots into rescue mode (also known as single mode: see <ulink
	url="&url-man;/bullseye/systemd/systemd.1.html">systemd(1)</ulink>),
	run <command>sudo systemctl edit rescue.service</command> and
	create a file saying just:
      </para>
      <programlisting>
[Service]
Environment=SYSTEMD_SULOGIN_FORCE=1
      </programlisting>
      <para>
	It might also (or instead) be useful to do this for the
	<literal>emergency.service</literal> unit, which is started
	<emphasis>automatically</emphasis> in the case of certain errors
	(see <ulink
	url="&url-man;/bullseye/systemd/systemd.special.7.html">systemd.special(7)</ulink>),
	or if <literal>emergency</literal> is added to the kernel
	command line (e.g. if the system can't be recovered by using the
	rescue mode).
      </para>
      <para>
	For background and a discussion on the security implications see
	<ulink url="&url-bts;/802211">#802211</ulink>.
      </para>
    </section>
  </section>

  <section id="obsolescense-and-deprecation">
    <title>Obsolescence and deprecation</title>
    <section id="noteworthy-obsolete-packages">
      <title>Noteworthy obsolete packages</title>
      <para>
	The following is a list of known and noteworthy obsolete
	packages (see <xref linkend="obsolete"/> for a description).
	<programlisting condition="fixme">
          TODO: Use the change-release information and sort by popcon

          This needs to be reviewed based on real upgrade logs (jfs)

          Alternative, another source of information is the UDD
          'not-in-testing' page:
          https://udd.debian.org/bapase.cgi?t=testing
	</programlisting>
      </para>
      <para>
	The list of obsolete packages includes:
	<itemizedlist>
          <listitem>
            <para>
              The <systemitem role="package">lilo</systemitem> package
              has been removed from bullseye. The successor of lilo as
              boot loader is <systemitem
              role="package">grub2</systemitem>.
            </para>
          </listitem>
          <listitem>
            <para>
              The Mailman mailing list manager suite version 3 is the only available
              version of Mailman in this release. Mailman has been split up into
              various components; the core is available in the package <systemitem
              role="package">mailman3</systemitem> and the full suite can be
              obtained via the <systemitem role="package">mailman3-full</systemitem>
              metapackage.
            </para>
            <para>
              The legacy Mailman version 2.1 is no longer available (this used to be
              the package <systemitem role="package">mailman</systemitem>). This branch
              depends on Python 2 which is no longer available in Debian.
            </para>
            <para>
              For upgrading instructions, please see <ulink
              url="https://docs.mailman3.org/en/latest/migration.html">the project's
              migration documentation.</ulink>
            </para>
          </listitem>
          <listitem>
            <para>
              The Linux kernel no longer provides
              <literal>isdn4linux</literal> (i4l) support.
              Consequently, the related userland packages <systemitem
              role="package">isdnutils</systemitem>, <systemitem
              role="package">isdnactivecards</systemitem>, <systemitem
              role="package">drdsl</systemitem> and <systemitem
              role="package">ibod</systemitem> have been removed from
              the archives.
            </para>
          </listitem>
          <listitem>
            <para>
              The deprecated libappindicator libraries are no longer
              provided. As a result, the related packages <systemitem
              role="package">libappindicator1</systemitem>, <systemitem
              role="package">libappindicator3-1</systemitem> and
              <systemitem
		  role="package">libappindicator-dev</systemitem> are no
              longer available. This is expected to cause dependency
              errors for third-party software that still depends on
              libappindicator to provide system tray and indicator
              support.
            </para>
            <para>
	      Debian is using <systemitem
	      role="package">libayatana-appindicator</systemitem> as the
	      successor of libappindicator. For technical background see
	      <ulink
		  url="https://lists.debian.org/debian-devel/2018/03/msg00506.html">this
	      announcement</ulink>.
            </para>
          </listitem>
          <listitem>
            <para>
	      Debian no longer provides <systemitem
	      role="package">chef</systemitem>. If you use Chef for configuration
	      management, the best upgrade path is probably to switch to using
	      the packages provided by <ulink url="https://www.chef.io/">Chef
	      Inc</ulink>.
            </para>
	    <para>
	      For background on the removal, see <ulink
	      url="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=963750">
	      the removal request</ulink>.
	    </para>
          </listitem>
	  <listitem>
            <para>
              Python 2 is already beyond its End Of Life, and will receive
              no security updates. It is not supported for running
              applications, and packages relying on it have either been
              switched to Python 3 or removed. However, Debian bullseye
              does still include a version of Python 2.7, as well as a
              small number of Python 2 build tools such as <systemitem
              role="package">python-setuptools</systemitem>. These are
              present only because they are required for a few application
              build processes that have not yet been converted to Python
              3.
            </para>
	  </listitem>

	</itemizedlist>
      </para>
    </section>

    <section id="deprecated-components">
      <title>Deprecated components for &releasename;</title>
      <para>
	With the next release of &debian; &nextrelease; (codenamed
	&nextreleasename;) some features will be deprecated. Users
	will need to migrate to other alternatives to prevent
	trouble when updating to &debian; &nextrelease;.
      </para>
      <para>
	This includes the following features:
      </para>

      <itemizedlist>
	<listitem>
          <para>
            The historical justifications for the filesystem layout with
            <filename>/bin</filename>, <filename>/sbin</filename>, and
            <filename>/lib</filename> directories separate from their
            equivalents under <filename>/usr</filename> no longer apply
            today; see the <ulink
            url="https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge">Freedesktop.org
            summary</ulink>. Debian bullseye will be the last Debian
            release that supports the non-merged-usr layout; for systems
            with a legacy layout that have been upgraded without a
            reinstall, the <systemitem
            role="package">usrmerge</systemitem> package exists to do
            the conversion if desired.
          </para>
	</listitem>

        <listitem>
	  <para>
           bullseye is the final Debian release to ship
           <command>apt-key</command>. Keys should be managed by
           dropping files into
           <filename>/etc/apt/trusted.gpg.d</filename> instead, in
           binary format as created by <command>gpg --export</command>
           with a <literal>.gpg</literal> extension, or ASCII armored
           with a <literal>.asc</literal> extension.
	  </para>
	  <para>
	    A replacement for <command>apt-key list</command> to
	    manually investigate the keyring is planned, but work has
	    not started yet.
	  </para>
	</listitem>

      </itemizedlist>
    </section>

  </section>
</chapter>
