# Slovak messages for release-notes.
# Copyright (C) 2009 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2012, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2017-06-16 15:21+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "sk"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "Inštalačný systém"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"Inštalátor Debianu (Debian Installer) je oficiálny inštalačný systém "
"Debianu. Ponúka rôzne spôsoby inštalácie. Metódy inštalácie dostupné pre váš "
"systém závisia na architektúre, ktorú používate."

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Obrazy inštalátora pre &releasename; nájdete spolu s Inštalačnou príručkou "
"na <ulink url=\"&url-installer;\">webe Debianu</ulink>."

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
#, fuzzy
#| msgid ""
#| "The Installation Guide is also included on the first CD/DVD of the "
#| "official Debian CD/DVD sets, at:"
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Inštalačná príručka sa tiež nachádza na prvom CD/DVD oficiálnej sady CD/DVD "
"Debianu na adrese:"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>jazyk</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Tiež si môžete pozrieť zoznam známych problémov s debian-installer - <ulink "
"url=\"&url-installer;index#errata\">errata</ulink>."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "Čo je nové v inštalačnom systéme?"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
#, fuzzy
#| msgid ""
#| "There has been a lot of development on the Debian Installer since its "
#| "previous official release with &debian; &oldrelease;, resulting in both "
#| "improved hardware support and some exciting new features."
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"Inštalátor Debianu prešiel množstvom vývoja od svojho prvého oficiálneho "
"vydania v &debian; &oldrelease;, čo prinieslo zlepšenú podporu hardvéru aj "
"niektoré vzrušujúce nové vlastnosti."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
#, fuzzy
#| msgid ""
#| "In these Release Notes we'll only list the major changes in the "
#| "installer.  If you are interested in an overview of the detailed changes "
#| "since &oldreleasename;, please check the release announcements for the "
#| "&releasename; beta and RC releases available from the Debian Installer's "
#| "<ulink url=\"&url-installer-news;\">news history</ulink>."
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"V týchto Poznámkach k vydaniu uvedieme iba hlavné zmeny inštalátora. Ak vás "
"zaujíma podrobný prehľad zmien od &oldreleasename;, pozrite si prosím "
"oznámenia o vydaní &releasename; beta a RC dostupné z <ulink url=\"&url-"
"installer-news;\">histórie noviniek</ulink> Inštalátora Debianu."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:120
msgid "Automated installation"
msgstr "Automatizovaná inštalácia"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:122
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""
"Niektoré zmeny spomenuté v predošlej sekcii tiež majú vplyv na podporu "
"automatickej inštalácie na základe vopred zostavených konfiguračných "
"súborov. To znamená, že ak máte existujúce vopred zostavené konfiguračné "
"súbory, ktoré fungovali v inštalátore &oldreleasename;, nemôžete očakávať, "
"že budú bezo zmien fungovať v novom inštalátore."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:129
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"<ulink url=\"&url-install-manual;\">Inštalačná príručka</ulink> obsahuje "
"samostatnú aktualizovanú prílohu s rozsiahlou dokumentáciou týkajúcu sa "
"vopred zostavenej konfigurácie."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:138
#, fuzzy
#| msgid "Automated installation"
msgid "Cloud installations"
msgstr "Automatizovaná inštalácia"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:140
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"bullseye for several popular cloud computing services including:"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:147
msgid "OpenStack"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:152
msgid "Amazon Web Services"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:157
msgid "Microsoft Azure"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:164
msgid ""
"Cloud images provide automation hooks via cloud-init and prioritize fast "
"instance startup using specifically optimized kernel packages and grub "
"configurations.  Images supporting different architectures are provided "
"where appropriate and the cloud team endeavors to support all features "
"offered by the cloud service."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:173
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:180
msgid "Container and Virtual Machine images"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:182
msgid ""
"Multi-architecture Debian bullseye container images are available on <ulink "
"url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the standard "
"images, a <quote>slim</quote> variant is available that reduces disk usage."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:188
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""

#~ msgid "Major changes"
#~ msgstr "Hlavné zmeny"

#~ msgid "Removed ports"
#~ msgstr "Odstránené porty"

#~ msgid ""
#~ "Support for the <literal>powerpc</literal> architecture has been removed."
#~ msgstr "Bola odstránená podpora architektúry <literal>powerpc</literal>."

#~ msgid "New ports"
#~ msgstr "Nové porty"

#~ msgid ""
#~ "Support for the <literal>mips64el</literal> architecture has been added "
#~ "to the installer."
#~ msgstr ""
#~ "Do inštalátora bola pridaná podpora architektúry <literal>mips64el</"
#~ "literal>."

#~ msgid "Graphical installer"
#~ msgstr "Grafická inštalácia"

#~ msgid ""
#~ "The graphical installer is now the default on supported platforms.  The "
#~ "text installer is still accessible from the very first menu, or if the "
#~ "system has limited capabilities."
#~ msgstr ""
#~ "Grafický inštalátor je teraz predvolený na všetkých podporovaných "
#~ "platformách. Textový inštalátor je stále dostupný z úplne prvého menu ak "
#~ "sú grafické schopnosti systému obmedzené."

#~ msgid "The kernel flavor has been bumped to <literal>i686</literal>"
#~ msgstr "Verzia jadra bola zvýšená na <literal>i686</literal>"

#~ msgid ""
#~ "The kernel flavor <literal>i586</literal> has been renamed to "
#~ "<literal>i686</literal>, since <literal>i586</literal> is no longer "
#~ "supported."
#~ msgstr ""
#~ "Verzia jadra <literal>i586</literal> bola premenovaná na <literal>i686</"
#~ "literal>, keďže <literal>i586</literal> už viac nie je podporovaná."

#~ msgid "Desktop selection"
#~ msgstr "Výber pracovného prostredia"

#~ msgid ""
#~ "Since jessie, the desktop can be chosen within tasksel during "
#~ "installation, and several desktops can be selected at the same time."
#~ msgstr ""
#~ "Od vydania Jessie je možné zvoliť pracovné prostredie pomocou tasksel "
#~ "počas inštalácie a zvoliť niekoľko pracovných prostredí súčasne."

#~ msgid "New languages"
#~ msgstr "Nové jazyky"

#~ msgid ""
#~ "Thanks to the huge efforts of translators, &debian; can now be installed "
#~ "in 75 languages, including English.  Most languages are available in both "
#~ "the text-based installation user interface and the graphical user "
#~ "interface, while some are only available in the graphical user interface."
#~ msgstr ""
#~ "Vďaka obrovskej snahe prekladateľov je možné &debian; nainštalovať v 75 "
#~ "jazykoch vrátane slovenčiny. Väčšina jazykov je dostupná ako v textovom "
#~ "inštalátore, tak aj v jeho grafickom rozhraní, ale niektoré sú dostupné "
#~ "iba v grafickom rozhraní."

#~ msgid ""
#~ "The languages that can only be selected using the graphical installer as "
#~ "their character sets cannot be presented in a non-graphical environment "
#~ "are: Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, "
#~ "Khmer, Malayalam, Marathi, Nepali, Punjabi, Tamil, Telugu, Tibetan, and "
#~ "Uyghur."
#~ msgstr ""
#~ "Jazyky, ktoré je možné vybrať iba pomocou grafického inštalátora, pretože "
#~ "ich znakové sady nie je možné vykresliť v negrafickom prostredí, sú: "
#~ "amharčina, bengálčina, dzongkä, gudžarátčina, hindčina, gruzínčina, "
#~ "kannadčina, khmérčina, malajálamčina, maráthčina, nepálčina, pandžábčina, "
#~ "tamilčina, telugčina, tibetčina a ujgurčina."

#~ msgid "UEFI boot"
#~ msgstr "Zavádzanie prostredníctvom UEFI"

#~ msgid ""
#~ "The &releasename; installer improves support for a lot of UEFI firmware "
#~ "and also supports installing on 32-bit UEFI firmware with a 64-bit kernel."
#~ msgstr ""
#~ "Inštalátor &releasename; vylepšuje podporu množstva firmvéru UEFI a tiež "
#~ "podporuje inštaláciu 32-bitového firmvéru UEFI s 64-bitovým jadrom."

#~ msgid "Note that this does not include support for UEFI Secure Boot."
#~ msgstr "Prosím, pamätajte, že toto nezahŕňa podporu UEFI Secure Boot."

#~ msgid "New method for naming network interfaces"
#~ msgstr "Nová schéma pomenovania sieťových rozhraní"

#~ msgid ""
#~ "The installer and the installed systems use a new standard naming scheme "
#~ "for network interfaces.  <literal>ens0</literal> or <literal>enp1s1</"
#~ "literal> (ethernet)  or <literal>wlp3s0</literal> (wlan) will replace the "
#~ "legacy <literal>eth0</literal>, <literal>eth1</literal>, etc.  See <xref "
#~ "linkend=\"new-interface-names\"/> for more information."
#~ msgstr ""
#~ "Inštalátor a nainštalované systémy používajú novú štandardnú schému "
#~ "pomenovania sieťových rozhraní. <literal>ens0</literal> alebo "
#~ "<literal>enp1s1</literal> (ethernet) alebo <literal>wlp3s0</literal> "
#~ "(wlan) nahradia staré <literal>eth0</literal>, <literal>eth1</literal> "
#~ "atď. Ďalšie informácie nájdete v <xref linkend=\"new-interface-names\"/>."

#~ msgid "Multi-arch images now default to <literal>amd64</literal>"
#~ msgstr "Multi-arch obrazy sú teraz predvolene pre <literal>amd64</literal>"

#~ msgid ""
#~ "Since 64-bit PCs have become more common, the default architecture on "
#~ "multi-arch images is now <literal>amd64</literal> instead of "
#~ "<literal>i386</literal>."
#~ msgstr ""
#~ "Keďže sú 64-bitové PC dnes najčastejšie, predvolenou architektúrou na "
#~ "obrazoch multi-arch je teraz <literal>amd64</literal> namiesto "
#~ "<literal>i386</literal>."

#~ msgid "Full CD sets removed"
#~ msgstr "Úplné sady CD odstránené"

#~ msgid ""
#~ "The full CD sets are not built anymore. The DVD images are still "
#~ "available as well as the netinst CD image."
#~ msgstr ""
#~ "Úplné sady CD odstránené sa už nezostavujú. Obrazy DVD sú naďalej k "
#~ "dispozícii ako aj obraz CD netinst."

#~ msgid ""
#~ "Also, as the installer now gives an easy choice of desktop selection "
#~ "within tasksel, only Xfce CD#1 remains as a single-CD desktop system."
#~ msgstr ""
#~ "Inštalátor teraz tiež umožňuje jednoduchú voľbu pracovného prostredia v "
#~ "rámci tasksel. Iba Xfce CD#1 zostáva ako systém s pracovným prostredím na "
#~ "jedinom CD."

#~ msgid "Accessibility in the installer and the installed system"
#~ msgstr "Prístupnosť v rámci inštalátora a nainštalovaného systému"

#~ msgid ""
#~ "The installer produces two beeps instead of one when booted with grub, so "
#~ "users can tell that they have to use the grub method of editing entries."
#~ msgstr ""
#~ "Pri zavedení pomocou grub inštalátor pípne dvakrát, nie len raz, aby "
#~ "používateľ vedel, že musí použiť spôsob úpravy položiek špecifický pre "
#~ "grub."

#~ msgid ""
#~ "MATE desktop is the default desktop when brltty or espeakup is used in "
#~ "debian-installer."
#~ msgstr ""
#~ "Pracovné prostredie MATE je predvolené pri použití brltty alebo espeakup "
#~ "v inštalátore Debianu."

#~ msgid "Added HTTPS support"
#~ msgstr "Pridaná podpora HTTPS"

#~ msgid ""
#~ "Support for HTTPS has been added to the installer, enabling downloading "
#~ "of packages from HTTPS mirrors."
#~ msgstr ""
#~ "Do inštalátora bola pridaná podpora HTTPS umožňujúca sťahovanie balíkov "
#~ "zo zrkadiel HTTPS."

#~ msgid ""
#~ "Support for the 'ia64' and 'sparc' architectures has been dropped from "
#~ "the installer since they have been removed from the archive."
#~ msgstr ""
#~ "Podpora architektúr „ia64“ a „sparc“ bola odstránená z inštalátora, "
#~ "pretože boli odstránené z archívu."

#~ msgid "New default init system"
#~ msgstr "Nový predvolený init systém"

#~ msgid ""
#~ "The installation system now installs systemd as the default init system."
#~ msgstr ""
#~ "Inštalačný systém teraz nainštaluje systemd ako predvolený init systém:"

#~ msgid "Replacing \"--\" by \"---\" for boot parameters"
#~ msgstr "Nahradenie „--“ v parametroch zavádzača systému za „---“"

#~ msgid ""
#~ "Due to a change on the Linux kernel side, the \"---\" separator is now "
#~ "used instead of the historical \"--\" to separate kernel parameters from "
#~ "userland parameters."
#~ msgstr ""
#~ "Kvôli zmene na strane linuxového jadra sa teraz na oddelenie parametrov "
#~ "jadra od parametrov používateľského priestoru teraz používa oddeľovač "
#~ "„---“  namiesto historického „--“."

#~ msgid "Languages added in this release:"
#~ msgstr "Jazyky pridané v tomto vydaní:"

#~ msgid "Tajik has been added to the graphical and text-based installer."
#~ msgstr "Tadžičtina boli pridané do grafického a textového inštalátora."

#~ msgid "Software speech support"
#~ msgstr "Podpora syntetizácie reči"

#~ msgid ""
#~ "&debian; can be installed using software speech, for instance by visually "
#~ "impaired people who do not use a Braille device.  This is triggered "
#~ "simply by typing <literal>s</literal> and <literal>Enter</literal> at the "
#~ "installer boot beep.  More than a dozen languages are supported."
#~ msgstr ""
#~ "&debian; je teraz možné nainštalovať za pomoci syntetizátora reči, čo sa "
#~ "môže hodiť napríklad ľuďom so zrakovým postihnutím, ktorí nepoužívajú "
#~ "braillovo zariadenie. To sa spúšťa jednoducho stlačením postupnosti "
#~ "<literal>s</literal> a <literal>Enter</literal> po pípnutí po štarte "
#~ "inštalátora. Sú podporované viac ako tri tucty jazykov."

#~ msgid "New supported platforms"
#~ msgstr "Nové podporované platformy"

#~ msgid "Intel Storage System SS4000-E"
#~ msgstr "Intel Storage System SS4000-E"

#~ msgid "Marvell's Kirkwood platform:"
#~ msgstr "Platforma Marvell Kirkwood:"

#~ msgid "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P and TS-419P"
#~ msgstr "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P aa TS-419P"

#~ msgid "Marvell SheevaPlug and GuruPlug"
#~ msgstr "Marvell SheevaPlug a GuruPlug"

#~ msgid "Marvell OpenRD-Base, OpenRD-Client and OpenRD-Ultimate"
#~ msgstr "Marvell OpenRD-Base, OpenRD-Client a OpenRD-Ultimate"

#~ msgid "HP t5325 Thin Client (partial support)"
#~ msgstr "Tenký klient HP t5325 (čiastočná podpora)"

#~ msgid ""
#~ "Welsh has been re-added to the graphical and text-based installer (it had "
#~ "been removed in &oldreleasename;)."
#~ msgstr ""
#~ "Waleština bola znovu pridaná do grafického a textového inštalátora (bola "
#~ "odstránená v &oldreleasename;)."

#~ msgid "Network configuration"
#~ msgstr "Konfigurácia siete"

#~ msgid "The installer now supports installation on IPv6-only networks."
#~ msgstr ""
#~ "Inštalačný systém teraz podporuje inštaláciu aj na sieťach, ktoré "
#~ "podporujú výhradne IPv6."

#~ msgid "It is now possible to install over a WPA-encrypted wireless network."
#~ msgstr ""
#~ "Je už možné nainštalovať cez bezdrôtové siete šifrované pomocou WPA."

#~ msgid ""
#~ "<literal>ext4</literal> is the default filesystem for new installations, "
#~ "replacing <literal>ext3</literal>."
#~ msgstr ""
#~ "<literal>ext4</literal> je predvolený súborový systém pri nových "
#~ "inštaláciách. Nahrádza <literal>ext3</literal>."

#~ msgid ""
#~ "The <literal>btrfs</literal> filesystem is provided as a technology "
#~ "preview."
#~ msgstr ""
#~ "Súborový systém <literal>Btrfs</literal> sa poskytuje ako ukážka "
#~ "technológie."

#~ msgid ""
#~ "It is now possible to install PCs in UEFI mode instead of using the "
#~ "legacy BIOS emulation."
#~ msgstr ""
#~ "Teraz je možné nainštalovať PC v režime UEFI namiesto použitia emulácie "
#~ "BIOSu v režime spätnej kompatibility."

#~ msgid ""
#~ "Asturian, Estonian, Icelandic, Kazakh and Persian have been added to the "
#~ "graphical and text-based installer."
#~ msgstr ""
#~ "Astúrčina, estónčina, islandčina, kazaština a perzština boli pridané do "
#~ "grafického a textového inštalátora."

#~ msgid ""
#~ "Thai, previously available only in the graphical user interface, is now "
#~ "available also in the text-based installation user interface too."
#~ msgstr ""
#~ "Thajčina, v minulosti dostupná iba v grafickom rozhraní, je teraz "
#~ "dostupná aj v textovom rozhraní inštalátora."

#~ msgid ""
#~ "Due to the lack of translation updates two languages were dropped in this "
#~ "release: Wolof and Welsh."
#~ msgstr ""
#~ "Z dôvodu nedostatku aktualizácií prekladu boli v tomto vydaní vypustené "
#~ "dva jazyky: wolof a waleština."

#~ msgid "Dropped platforms"
#~ msgstr "Už nepodporované platformy"

#~ msgid ""
#~ "Support for the Alpha ('alpha'), ARM ('arm') and HP PA-RISC ('hppa')  "
#~ "architectures has been dropped from the installer.  The 'arm' "
#~ "architecture is obsoleted by the ARM EABI ('armel') port."
#~ msgstr ""
#~ "Podpora architektúr Alpha („alpha“), ARM („arm“) a HP PA-RISC („hppa“) "
#~ "bola z inštalátora vypustená. Architektúra „arm“ bola nahradená portom "
#~ "ARM EABI („armel“)."

#~ msgid "Support for kFreeBSD"
#~ msgstr "Podpora kFreeBSD"

#~ msgid ""
#~ "The installer can be used to install the kFreeBSD instead of the Linux "
#~ "kernel and test the technology preview. To use this feature the "
#~ "appropriate installation image (or CD/DVD set) has to be used."
#~ msgstr ""
#~ "Inštalátor možno použiť na nainštalovanie kFreeBSD namiesto jadra Linuxu "
#~ "a na odskúšanie tejto ukážky technológie. Aby ste mohli využiť túto "
#~ "možnosť, musíte použiť vhodný inštalačný obraz (alebo sadu CD/DVD)."

#~ msgid "GRUB 2 is the default bootloader"
#~ msgstr "GRUB 2 je predvolený zavádzač systému"

#~ msgid ""
#~ "The bootloader that will be installed by default is <systemitem role="
#~ "\"package\">grub-pc</systemitem> (GRUB 2)."
#~ msgstr ""
#~ "Zavádzač systému, ktorý sa štandardne nainštaluje je <systemitem role="
#~ "\"package\">grub-pc</systemitem> (GRUB 2)."

#~ msgid "Help during the installation process"
#~ msgstr "Pomoc počas inštalácie"

#~ msgid ""
#~ "The dialogs presented during the installation process now provide help "
#~ "information. Although not currently used in all dialogs, this feature "
#~ "would be increasingly used in future releases. This will improve the user "
#~ "experience during the installation process, especially for new users."
#~ msgstr ""
#~ "Dialógy, ktoré sa zobrazujú počas inštalácie teraz poskytujú informácie "
#~ "Pomocníka. Hoci momentálne nie sú použité vo všetkých dialógoch, táto "
#~ "vlastnosť sa bude postupne viac využívať v ďalších vydaniach. Pomôže to "
#~ "zlepšiť používateľský zážitok z inštalácie, obzvlášť novým používateľom."

#~ msgid "Installation of Recommended packages"
#~ msgstr "Inštalácia odporúčaných balíkov"

#~ msgid ""
#~ "The installation system will install all recommended packages by default "
#~ "throughout the process except for some specific situations in which the "
#~ "general setting gives undesired results."
#~ msgstr ""
#~ "Inštalačný systém bude odteraz štandardne inštalovať všetky odporúčané "
#~ "balíky s výnimkou niektorých špecifických situácií, v ktorých táto "
#~ "štandardná voľba vedie k nežiadúcim výsledkom."

#~ msgid "Automatic installation of hardware-specific packages"
#~ msgstr "Automatická inštalácia balíkov určitého hardvéru"

#~ msgid ""
#~ "The system will automatically select for installation hardware-specific "
#~ "packages when they are appropriate. This is achieved through the use of "
#~ "<literal>discover-pkginstall</literal> from the <systemitem role=\"package"
#~ "\">discover</systemitem> package."
#~ msgstr ""
#~ "Systém automaticky zvolí na inštaláciu balíky určitého hardvéru, keď je "
#~ "to vhodné. Zabezpečuje to <literal>discover-pkginstall</literal> z balíka "
#~ "<systemitem role=\"package\">discover</systemitem>."

#~ msgid "Support for installation of previous releases"
#~ msgstr "Podpora inštalácie predošlých vydaní"

#~ msgid ""
#~ "The installation system can be also used for the installation of previous "
#~ "release, such as &oldreleasename;."
#~ msgstr ""
#~ "Inštalačný systém je teraz možné použiť aj na nainštalovanie predošlého "
#~ "vydania, ako &oldreleasename;."

#~ msgid "Improved mirror selection"
#~ msgstr "Vylepšený výber zrkadiel"

#~ msgid ""
#~ "The installation system provides better support for installing both "
#~ "&releasename; as well as &oldreleasename; and older releases (through the "
#~ "use of archive.debian.org). In addition, it will also check that the "
#~ "selected mirror is consistent and holds the selected release."
#~ msgstr ""
#~ "Inštalačný systém poskytuje lepšiu podporu inštalácie &releasename; ako "
#~ "aj &oldreleasename; a starších vydaní (prostredníctvom archive.debian."
#~ "org). Naviac skontroluje konzistentnosť vybraného zrkadla a či obsahuje "
#~ "zvolené vydanie."

#~ msgid "Changes in partitioning features"
#~ msgstr "Zmeny v možnostiach rozdelenia disku"

#~ msgid ""
#~ "This release of the installer supports the use of the ext4 file system "
#~ "and it also simplifies the creation of RAID, LVM and crypto protected "
#~ "partitioning systems. Support for the reiserfs file system is no longer "
#~ "included by default, although it can be optionally loaded."
#~ msgstr ""
#~ "Toto vydanie inštalátora podporuje použitie súborového systému ext4 a "
#~ "tiež zjednodušuje tvornu RAID, LVM a šifrovaných súborových systémov. "
#~ "Štandardne už neobsahuje podporu súborového systému reiserfs, hoci je "
#~ "možné ju voliteľne načítať."

#~ msgid "Support for loading firmware debs during installation"
#~ msgstr "Podpora načítania balíkov .deb s firmvérom počas inštalácie"

#~ msgid ""
#~ "It is now possible to load firmware package files from the installation "
#~ "media in addition to removable media, allowing the creation of PXE images "
#~ "and CDs/DVDs with included firmware packages."
#~ msgstr ""
#~ "Odteraz je možné načítavať balíky s firmware aj z inštalačnch médií okrem "
#~ "prenosných médií. To umožňuje tvorbu PXE obrazov a CD/CVD, ktorá obsahujú "
#~ "balíky s firmware."

#~ msgid ""
#~ "Starting with Debian &release;, non-free firmware has been moved out of "
#~ "main.  To install Debian on hardware that needs non-free firmware, you "
#~ "can either provide the firmware yourself during installation or use pre-"
#~ "made non-free CDs/DVDs which include the firmware. See the <ulink url="
#~ "\"http://www.debian.org/distrib\">Getting Debian section</ulink> on the "
#~ "Debian website for more information."
#~ msgstr ""
#~ "Počínajúc Debian &release;, neslobodný firmware bol presunutý preč z "
#~ "main. Ak chcete nainštalovať Debian na hardvéri, ktorý vyžaduje "
#~ "neslobodný firmware, môžete buď firmware sami poskytnúť počas inštalácie, "
#~ "alebo použiť predpripravené neslobodné CD/DVD, ktoré tento firmware "
#~ "obsahujú. Ďalšie informácie nájdete na webe Debianu v časti <ulink url="
#~ "\"http://www.debian.org/distrib\">Ako získať Debian</ulink>."

#~ msgid "Improved localisation selection"
#~ msgstr "Vylepšený výber lokalizácie"

#~ msgid ""
#~ "The selection of localisation-related values (language, location and "
#~ "locale settings) is now less interdependent and more flexible. Users will "
#~ "be able to customize the system to their localisation needs more easily "
#~ "while still make it comfortable to use for users that want to select the "
#~ "locale most common for the country they reside in."
#~ msgstr ""
#~ "Výber hodnôt súvisiacich s lokalizáciou (jazyk, umiestnenie a nastavenie "
#~ "locale) teraz menej závisia na sebe navzájom a sú flexibilnejšie. "
#~ "Používatelia budú schopní prispôsobiť si systém jednoduchšie, pričom to "
#~ "bude stále pohodlné pre používateľov, ktorí si chcú zvoliť locale "
#~ "najbežnejšie pre krajinu, v ktorej sídlia."

#~ msgid ""
#~ "Additionally, the consequences of localisation choices (such as timezone, "
#~ "keymap and mirror selection) are now more obvious to the user."
#~ msgstr ""
#~ "Naviac sú teraz dôsledky lokalizačných volieb (ako časové pásmo, mapa "
#~ "klávesnice a výber zrkadla) pre používateľa viditeľnejšie."

#~ msgid "Live system installation"
#~ msgstr "Inštalácia live systému"

#~ msgid ""
#~ "The installer now supports live systems in two ways. First, an installer "
#~ "included on live system media can use the contents of the live system in "
#~ "place of the regular installation of the base system. Second, the "
#~ "installer may now be launched while running the live system, allowing the "
#~ "user to do other things with the live system during the install. Both "
#~ "features are built into the Debian Live images offered at <ulink url="
#~ "\"http://cdimage.debian.org/\" />."
#~ msgstr ""
#~ "Inštalátor teraz podporuje live systémy dvomi spôsobmi. Za prvé, "
#~ "inštalátor na médiu s live systémom dokáže používať obsah live systému "
#~ "namiesto bežnej inštalácie základného systému. Za druhé, inštalátor je "
#~ "teraz možné spustiť z prostredia bežiaceho live systému, čo umožňuje "
#~ "používateľovi pracovať s live systémom počas inštalácie. Obe možnosti sú "
#~ "zabudované do live obrazov Debianu, ktoré ponúkame na <ulink url=\"http://"
#~ "cdimage.debian.org/\" />."
